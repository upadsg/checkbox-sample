package com.jedi.exviews.activities.checkbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.jedi.exviews.R;

import java.util.ArrayList;
import java.util.List;

public class CheckBoxActivity extends AppCompatActivity {

    private List<CheckBox> checkBoxes;
    private Button buttonListar;
    private TextView textViewSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);

        this.textViewSelected = (TextView)findViewById(R.id.textViewSelected);
        loadCheckBoxes();
        loadButtonListar();
    }

    private void loadButtonListar() {
        this.buttonListar = (Button)findViewById(R.id.buttonListar);
        this.buttonListar.setOnClickListener(new ButtonClickListener(this.checkBoxes, this.textViewSelected));
    }

    private void loadCheckBoxes() {
        this.checkBoxes = new ArrayList<>();
        this.checkBoxes.add((CheckBox)findViewById(R.id.checkboxJava));
        this.checkBoxes.add((CheckBox)findViewById(R.id.checkboxCSharp));
        this.checkBoxes.add((CheckBox)findViewById(R.id.checkboxJavaScript));
        this.checkBoxes.add((CheckBox)findViewById(R.id.checkboxCPlusPlus));
        this.checkBoxes.add((CheckBox)findViewById(R.id.checkboxVisualBasicDotNet));
    }

    private class ButtonClickListener implements View.OnClickListener{

        private List<CheckBox> checkBoxes;
        private TextView textView;

        private ButtonClickListener(List<CheckBox> checkBoxes, TextView textView) {
            this.checkBoxes = checkBoxes;
            this.textView = textView;
        }

        @Override
        public void onClick(View v) {
            String result = "";

            for(CheckBox ch : this.checkBoxes){
                if(ch.isChecked()){
                    result += ch.getText() + "\n";
                }
            }

            this.textView.setText(result);
        }
    }
}
