package com.jedi.exviews.activities.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jedi.exviews.R;
import com.jedi.exviews.activities.checkbox.CheckBoxActivity;

public class MainActivity extends AppCompatActivity {

    Button buttonCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.buttonCheckBox = (Button)findViewById(R.id.buttonGotoCheckBoxes);
        this.buttonCheckBox.setOnClickListener(new ButtonCheckBoxesClickListener());
    }

    private class ButtonCheckBoxesClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), CheckBoxActivity.class);
            v.getContext().startActivity(intent);
        }
    }
}
